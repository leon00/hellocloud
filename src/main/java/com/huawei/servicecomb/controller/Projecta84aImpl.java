package com.huawei.servicecomb.controller;


import javax.ws.rs.core.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.apache.servicecomb.provider.rest.common.RestSchema;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.CseSpringDemoCodegen", date = "2019-03-26T09:48:20.947Z")

@RestSchema(schemaId = "projecta84a")
@RequestMapping(path = "/rest", produces = MediaType.APPLICATION_JSON)
public class Projecta84aImpl {

    @Autowired
    private Projecta84aDelegate userProjecta84aDelegate;


    @RequestMapping(value = "/helloworld",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    public String helloworld( @RequestParam(value = "name", required = true) String name){

        return userProjecta84aDelegate.helloworld(name);
    }

}
